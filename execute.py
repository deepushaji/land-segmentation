import numpy as np
import tensorflow as tf
from tensorflow import keras
from keras.losses import binary_crossentropy
import keras.backend as K
import matplotlib.pyplot as plt
import numpy as np
import sys
from skimage.util import montage as montage2d
import os
from skimage.io import imread
from skimage.transform import resize

def dice_coef(y_true, y_pred, smooth=1):
    intersection = K.sum(y_true * y_pred, axis=[1,2,3])
    union = K.sum(y_true, axis=[1,2,3]) + K.sum(y_pred, axis=[1,2,3])
    return K.mean( (2. * intersection + smooth) / (union + smooth), axis=0)

def dice_p_bce(in_gt, in_pred):
    return 0.05*binary_crossentropy(in_gt, in_pred) - dice_coef(in_gt, in_pred)

def true_positive_rate(y_true, y_pred):
    return K.sum(K.flatten(y_true)*K.flatten(K.round(y_pred)))/K.sum(y_true)

model = keras.models.load_model('full_best_model.h5',custom_objects={'dice_p_bce': dice_p_bce,'dice_coef':dice_coef,'true_positive_rate':true_positive_rate})

model.summary()


def batch_img_gen():
    out_img, out_seg = [], []
    for filename in os.listdir('testdata')[:1]:
        print(os.path.join('testdata/', filename))
        img_data = imread(sys.argv[1])
        # img_data = resize(img_data, ((img_data.shape[0]/img_data.shape[0])*300 , (img_data.shape[0]/img_data.shape[0])*300),
        #                anti_aliasing=True)
        out_img += [img_data]
    #    out_seg += [np.expand_dims(rows_to_segmentation(img_data, c_df), -1)]
                
    return (np.stack(out_img, 0)/255.0).astype(np.float32), (np.stack(out_img, 0)/255.0).astype(np.float32) #np.stack(out_seg, 0).astype(np.float32)

t_x, t_y = batch_img_gen()

    
print('x', t_x.shape, t_x.dtype, t_x.min(), t_x.max())
print('y', t_y.shape, t_y.dtype, t_y.min(), t_y.max())
pred_y = model.predict(t_x)

fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize = (24, 8))
montage_rgb = lambda x: np.stack([montage2d(x[:, :, :, i]) for i in range(x.shape[3])], -1)
ax1.imshow(montage_rgb(t_x))
ax3.imshow(montage2d(pred_y[:, :, :, 0]), cmap = 'bone_r')
ax3.set_title('Prediction')
fig.savefig('pred_fig.png', dpi=300)

print("come on")